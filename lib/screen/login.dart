import 'package:flutarc/locator.dart';
import 'package:flutarc/store/auth_store.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  final _authStore = locator<AuthStore>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextFormField(
              initialValue: _authStore.name,
              onChanged: _authStore.inputName,
              decoration: InputDecoration(labelText: "Name"),
            ),
            SizedBox(height: 30),
            RaisedButton(
              onPressed: () => _authStore.login(),
              color: Theme.of(context).primaryColor,
              child: Text(
                'Login',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
