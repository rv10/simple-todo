import 'package:flutarc/locator.dart';
import 'package:flutarc/model/todo.dart';
import 'package:flutarc/store/todo_store.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class TodoScreen extends StatelessWidget {
  final _todoStore = locator<TodoStore>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Todo App"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  child: TextFormField(
                    onChanged: _todoStore.inputName,
                    decoration: InputDecoration(
                      labelText: "Todo",
                    ),
                  ),
                ),
                SizedBox(width: 20),
                SizedBox(
                  height: 55,
                  width: 60,
                  child: RaisedButton(
                    child: Icon(Icons.add),
                    onPressed: () => _todoStore.addTodo(),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            Divider(),
            SizedBox(height: 20),
            Expanded(
              child: Observer(
                builder: (context) => ListView.separated(
                  separatorBuilder: (context, index) => Divider(),
                  itemCount: _todoStore.todos.length,
                  itemBuilder: (context, index) {
                    Todo todo = _todoStore.todos[index];
                    return ListTile(
                      title: Text('${todo.title}'),
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () => _todoStore.delete(todo.id),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
