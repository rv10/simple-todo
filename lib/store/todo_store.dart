import 'package:flutarc/model/todo.dart';
import 'package:get/get.dart';
import 'package:mobx/mobx.dart';
import 'package:uuid/uuid.dart';

part 'todo_store.g.dart';

class TodoStore = TodoBase with _$TodoStore;

abstract class TodoBase with Store {
  // final _storage = locator<StorageService>();
  var uuid = Uuid();

  @observable
  String title = '';

  @observable
  ObservableList<Todo> todos = ObservableList<Todo>();

  @action
  inputName(String val) => title = val;

  @action
  addTodo() {
    if (title.trim().isEmpty) {
      Get.rawSnackbar(message: "please add a todo content!");
      return;
    }
    todos.add(Todo(id: uuid.v1(), title: title));
  }

  @action
  delete(String id) => todos.removeWhere((element) => element.id == id);
}
