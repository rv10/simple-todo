// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'todo_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TodoStore on TodoBase, Store {
  final _$titleAtom = Atom(name: 'TodoBase.title');

  @override
  String get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  final _$todosAtom = Atom(name: 'TodoBase.todos');

  @override
  ObservableList<Todo> get todos {
    _$todosAtom.reportRead();
    return super.todos;
  }

  @override
  set todos(ObservableList<Todo> value) {
    _$todosAtom.reportWrite(value, super.todos, () {
      super.todos = value;
    });
  }

  final _$TodoBaseActionController = ActionController(name: 'TodoBase');

  @override
  dynamic inputName(String val) {
    final _$actionInfo =
        _$TodoBaseActionController.startAction(name: 'TodoBase.inputName');
    try {
      return super.inputName(val);
    } finally {
      _$TodoBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addTodo() {
    final _$actionInfo =
        _$TodoBaseActionController.startAction(name: 'TodoBase.addTodo');
    try {
      return super.addTodo();
    } finally {
      _$TodoBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic delete(String id) {
    final _$actionInfo =
        _$TodoBaseActionController.startAction(name: 'TodoBase.delete');
    try {
      return super.delete(id);
    } finally {
      _$TodoBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
title: ${title},
todos: ${todos}
    ''';
  }
}
