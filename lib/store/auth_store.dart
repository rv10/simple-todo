import 'package:flutarc/locator.dart';
import 'package:flutarc/model/user.dart';
import 'package:flutarc/screen/todo.dart';
import 'package:flutarc/service/storage.dart';
import 'package:get/get.dart';
import 'package:mobx/mobx.dart';
import 'package:uuid/uuid.dart';

part 'auth_store.g.dart';

class AuthStore = AuthBase with _$AuthStore;

abstract class AuthBase with Store {
  final _storage = locator<StorageService>();
  var uuid = Uuid();

  @observable
  String name = '';

  @action
  inputName(String val) => name = val;

  @action
  login() async {
    if (name.trim().isEmpty) {
      Get.rawSnackbar(message: "Please Enter a name");
      return;
    }

    try {
      _storage.setUser(User(id: uuid.v1(), name: name));
      Get.off(TodoScreen());
    } catch (e) {
      Get.rawSnackbar(message: "$e");
    }
  }
}
