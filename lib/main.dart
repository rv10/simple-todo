import 'package:flutarc/locator.dart';
import 'package:flutarc/model/user.dart';
import 'package:flutarc/screen/login.dart';
import 'package:flutarc/service/storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter<User>(UserAdapter());
  // Hive.registerAdapter<Todo>(TodoAdapter());
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

   @override
  void initState() {
    locator<StorageService>().initDB();
    super.initState();
  }

  @override
  void dispose() {
    locator<StorageService>().closeDB();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        inputDecorationTheme: InputDecorationTheme(
          border: OutlineInputBorder(
            borderSide: BorderSide(
              color: Color.fromARGB(255, 52, 146, 131),
              width: 1,
            ),
          ),
        ),
      ),
      home: LoginScreen(),
    );
  }
}
