import 'package:flutarc/model/todo.dart';
import 'package:flutarc/model/user.dart';
import 'package:hive/hive.dart';

class StorageService {
  static const _userBox = '_userBox';
  static const _todoBox = '_todoBox';
  Box<dynamic> _user;
  Box<dynamic> _todo;

  initDB() async {
    _user = await Hive.openBox<dynamic>(_userBox);
    _todo = await Hive.openBox<dynamic>(_todoBox);
    print('hive initialized');
  }

  closeDB() => Hive.close();

  deleteDB() => Hive.deleteFromDisk();

  String getUser() => _user.get(_userBox);

  String getTodo() => _todo.get(_todoBox);

  Future<void> setToken(Todo todo) => _todo.put(_todoBox, todo);

  Future<void> setUser(User user) => _user.put(_userBox, user);
}
