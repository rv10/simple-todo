import 'package:flutarc/service/storage.dart';
import 'package:flutarc/store/auth_store.dart';
import 'package:flutarc/store/todo_store.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton(() => StorageService());

  locator.registerLazySingleton(() => AuthStore());
  locator.registerLazySingleton(() => TodoStore());
}
